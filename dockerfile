FROM openjdk:8
EXPOSE 3000
ADD target/java-tomcat-maven-example.war java-tomcat-maven-example.war
ENTRYPOINT ["java","-war","/java-tomcat-maven-example.war"]

